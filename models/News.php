<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int    $id
 * @property string $alias       ЧПУ
 * @property string $title       Название
 * @property string $description Описание
 * @property string $content     Контент
 * @property int    $enabled     Активность
 * @property int    $created_at  Дата создания
 * @property int    $updated_at  Дата изменения
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['content'], 'string'],
            [['enabled', 'created_at', 'updated_at'], 'integer'],
            [['alias', 'title', 'description'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => \Yii::t('news', 'ID'),
            'alias'       => \Yii::t('news', 'Alias'),
            'title'       => \Yii::t('news', 'Title'),
            'description' => \Yii::t('news', 'Description'),
            'content'     => \Yii::t('news', 'Content'),
            'enabled'     => \Yii::t('news', 'Enabled'),
            'created_at'  => \Yii::t('news', 'Created At'),
            'updated_at'  => \Yii::t('news', 'Updated At'),
        ];
    }

    /**
     * @param $alias
     *
     * @return null|\yii\db\ActiveRecord
     */
    public static function findByAlias($alias)
    {
        return static::find()->where(['alias' => $alias])->one();
    }

}
