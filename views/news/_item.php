<?php

use yii\helpers\Html;
use yii\helpers\Url;


/** @var \app\models\News $model */
?>

<div class="row">
    <div class="col-xs-12">
        <a href="<?= Url::to(['/news/view', 'alias' => $model->alias]) ?>">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= Html::encode($model->title); ?>
                    <span class="pull-right"><?=\Yii::$app->formatter->asDatetime($model->created_at)?></span>
                </div>
                <div class="panel-body">
                    <?= Html::encode($model->description); ?>
                </div>
            </div>
        </a>
    </div>
</div>


