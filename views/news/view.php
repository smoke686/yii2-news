<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('news', 'News'), 'url' => ['/news/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <h4><?=\Yii::$app->formatter->asDatetime($model->created_at)?></h4>
    <div class="row">
        <div class="col-xs-12">
            <p><?= Html::encode($model->content); ?></p>
        </div>
    </div>
</div>
