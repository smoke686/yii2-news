<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title                   = \Yii::t('news', 'Update News: ') . $model->title;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('news', 'News'), 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['/news/view', 'alias' => $model->alias]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Update');
?>
<div class="admin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
