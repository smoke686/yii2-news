<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = \Yii::t('news', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('news', 'Create News'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'alias',
            'title',
            'description',
            'content:ntext',
            [
                'label' => \Yii::t('news', 'Created At'),
                'value' => function ($model) {
                    return \Yii::$app->formatter->asDatetime($model->created_at);
                },
            ],
            [
                'label' => \Yii::t('news', 'Updated At'),
                'value' => function ($model) {
                    return \Yii::$app->formatter->asDatetime($model->created_at);
                },
            ],
            [
                'label' => \Yii::t('news', 'Enabled'),
                'value' => function ($model) {
                    return $model->enabled ? \Yii::t('news', 'Active') : \Yii::t('news', 'Disable');
                },
            ],
            [
                'class'   => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(
                            Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']),
                            Url::to(['/news/view', 'alias' => $model->alias])
                        );
                    },
                ],
            ],
        ],
    ]); ?>
</div>
