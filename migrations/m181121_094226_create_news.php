<?php

use yii\db\Migration;

/**
 * Class m181121_094226_create_news
 */
class m181121_094226_create_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(255)->notNull()->unique()->comment('ЧПУ'),
            'title' => $this->string(255)->notNull()->comment('Название'),
            'description' => $this->string(255)->null()->comment('Описание'),
            'content' => $this->text()->null()->comment('Контент'),
            'enabled' => $this->boolean()->defaultValue(0)->comment('Активность'),
            'created_at' => $this->integer()->null()->comment('Дата создания'),
            'updated_at' => $this->integer()->null()->comment('Дата изменения')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
