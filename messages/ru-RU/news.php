<?php

return [
    'News'          => 'Новости',
    'Create News'   => 'Создать новость',
    'Update News: ' => 'Изменить новость: ',
    'Alias'         => 'ЧПУ',
    'Title'         => 'Заголовок',
    'Description'   => 'Описание',
    'Content'       => 'Содержание',
    'Enabled'       => 'Активность',
    'Created At'    => 'Дата создания',
    'Updated At'    => 'Дата изменения',
    'Active'        => 'Активна',
    'Disable'       => 'Не активна',
];