<?php

return [
    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    'Save'   => 'Сохранить',
];